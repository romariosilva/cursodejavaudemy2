package metodos;

abstract class Eletrodomestico {
	
	public abstract void ligar (String eletro) ; 
	

	public abstract void desligar (String eletro) ; 
	
	
	
	
}

public class Testes_Abs extends Eletrodomestico {

	public static void main(String[] args) {
		
		Testes_Abs s = new Testes_Abs(); 
		s.ligar(" Som"); 
	

	}
	
	public void desligar(String eletro) {
		System.out.println("Desligado "+eletro);
	}
	
	public void ligar(String eletro) {
		System.out.println("Ligado "+eletro);
	}

}
